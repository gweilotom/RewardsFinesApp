import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { OverlayService } from '../overlay/services/overlay.service';
import { ConfigService } from './config.service';
import { LanguageService } from './language.service';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    private title: String;

    private configLoaded: boolean;
    private languagesLoaded: boolean;

    private loggedIn: boolean;
    private executive: boolean;
    
    constructor(
        private configService: ConfigService,
        private languageService: LanguageService,
        private overlayService: OverlayService,
        private router: Router
    ) {
        this.title = 'R.C. Linz';

        this.configLoaded = false;
        this.languagesLoaded = false;

        this.loggedIn = false;
        this.executive = false;
    }

    public initAppData(aCallback: Function): void {
        this.overlayService.show();
        this.configService.loadConfiguration((success) => this.initConfigComplete(success, aCallback));
        this.languageService.loadLanguages((success) => this.initLanguageComplete(success, aCallback));
    }

    private initConfigComplete(aSuccess: boolean, aCallback: Function): void {
        this.configLoaded = aSuccess;
        if (this.configLoaded && this.languagesLoaded) {
            this.overlayService.hide();
            aCallback(true);
        }
    }

    private initLanguageComplete(aSuccess: boolean, aCallback: Function): void {
        this.languagesLoaded = aSuccess;
        if (this.languagesLoaded && this.configLoaded) {
            this.overlayService.hide();
            aCallback(true);
        }
    }

    public getTitle(): String {
        return this.title;
    }

    public setTitle(aTitle: String): void {
        this.title = aTitle;
    }

    public isLoggedIn(): boolean {
        return this.loggedIn;
    }

    public setLoggedIn(aLoggedIn: boolean): void {
        this.loggedIn = aLoggedIn;
    }

    public isExecutive(): boolean {
        return this.executive;
    }

    public setExecutive(aExecutive: boolean): void {
        this.executive = aExecutive;
    }

    public getWebServiceUrl(): String {
        return this.configService.getWebServiceUrl();
    }

    public getSupportedLanguages(): Map<String, String> {
        return this.languageService.getSupportedLanguages();
    }

    public getTranslation(aKey: String, aLanguage = this.languageService.getSelectedLanguage()): String {
        return this.languageService.getTranslation(aKey, aLanguage);
    }

    public getSelectedLanguage(): String {
        return this.languageService.getSelectedLanguage();
    }

    public setLanguage(aNewLanguage: String) {
        this.languageService.changeLanguage(aNewLanguage);
    }

    public showSpinner(): void {
        this.overlayService.show();
    }

    public hideSpinner(): void {
        this.overlayService.hide();
    }

    public goTo(aRoute: string): void {
        this.router.navigateByUrl(aRoute);
    }
}