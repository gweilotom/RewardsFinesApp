import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {

    private selectedLanguage: String;
    private supportedLanguages: Map<String, String>;
    private translations: Map<String, Map<String, String>>;

    constructor(private http: HttpClient) {}

    public loadLanguages(aCallback: Function): void {
        this.http.get<any>('./assets/resources/languages.json').subscribe(data => {
            const languages = data.languages;
            this.supportedLanguages = new Map<String, String>();
            const languageKeys = Object.keys(languages);
            for (let key in languageKeys) {
                this.supportedLanguages.set(languageKeys[key], languages[languageKeys[key]]);
            }
            this.selectedLanguage = 'en';
            this.loadTranslations(data.translations, aCallback);
        }, error => {
            console.error('Error occurred while importing languages: ', error);
            aCallback(false);
        })
    }

    private loadTranslations(rawData: any, aCallback: Function): void {
        this.translations = new Map<String, Map<String, String>>();
        const translationKeys = Object.keys(rawData);
        for (let key in translationKeys) {
            let subKeys = Object.keys(rawData[translationKeys[key]]);
            let subMap = new Map<String, String>();
            let dataSection = rawData[translationKeys[key]];
            for (let subKey in subKeys) {
                subMap.set(subKeys[subKey], dataSection[subKeys[subKey]]);
            }
            this.translations.set(translationKeys[key], subMap);
        }
        aCallback(true);
    }

    public getSupportedLanguages(): Map<String, String> {
        return this.supportedLanguages;
    }

    public getSelectedLanguage(): String {
        return this.selectedLanguage;
    }

    public changeLanguage(aLanguage: String): void {
        this.selectedLanguage = aLanguage;
    }

    public getTranslation(aKey: String, aLanguage = this.selectedLanguage): String {
        let value = aKey;
        const map = this.translations.get(aKey);
        if (map != undefined || map != null) {
            value = map.get(aLanguage);
            if(value == undefined || value == null || value == '') {
                return aKey;
            }
        }
        return value;
    }
}
