import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {
    
    private protocol: String;
    private webserver: String;
    private port: String;
    private webServiceUrl: String;

    constructor(private http: HttpClient) {}

    public loadConfiguration(callback: Function): void {
      this.http.get<any>('./assets/resources/config.json').subscribe(data => {
          this.protocol = data.protocol;
          this.webserver = data.webserver;
          this.port = data.port;
          this.webServiceUrl = this.buildWebServiceUrl();
          callback(true);
      }, error => {
          console.error('Error occurred while loading config: ', error);
          callback(false);
      });
    }

    private buildWebServiceUrl(): String {
        return this.protocol + '://' + this.webserver + ':' + this.port + '/';
    }

    public getWebServiceUrl(): String {
        return this.webServiceUrl;
    }
}
