import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { FormGroup } from '@angular/forms';
import { GlobalService } from 'src/app/global/services/global.service';
import { BaseService } from "../../base/services/base.service";

@Injectable({
    providedIn: 'root'
})
export class LoginService extends BaseService {

    private salt = '1.ORSCLinz';

    constructor(
        protected globalService: GlobalService,
        private http: HttpClient
    ) {
        super(globalService);
    }

    public login(aLoginForm: FormGroup): void {
        this.showSpinner();
        if (aLoginForm.get('useremail').value.includes('@')) {
            const payload: any = {
                email: aLoginForm.get('useremail').value,
                password: btoa(aLoginForm.get('password').value + ':' + this.salt)
            };
            this.loginWithEmail(payload);
        } else {
            const payload: any = {
                username: aLoginForm.get('useremail').value,
                password: btoa(aLoginForm.get('password').value + ':' + this.salt)
            };
            this.loginWithUsername(payload);
        }
    }

    private loginWithEmail(aPayload: any): void {
        const request = this.globalService.getWebServiceUrl() + 'loginWithEmail';
        this.http.post<JSON>(request, aPayload).subscribe(data => {
            this.processLoginResponse(data);
        }, error => {
            console.error('Error occurred during log in: ', error);
            this.hideSpinner();
        });
    }

    private loginWithUsername(aPayload: any): void {
        const request = this.globalService.getWebServiceUrl() + 'loginWithUsername';
        this.http.post<JSON>(request, aPayload).subscribe(data => {
            this.processLoginResponse(data);
        }, error => {
            console.error('Error occured during log in: ', error);
            this.hideSpinner();
        });
    }

    public setLoggedIn(aState: boolean): void {
        this.globalService.setLoggedIn(aState);
    }

    private processLoginResponse(aResponse: any): void {
        this.globalService.setLoggedIn(true);
        this.globalService.setExecutive(aResponse.executive);
        if (aResponse.language) {
            this.checkChangeLanguage(aResponse.language, () => {});
        }
        this.globalService.goTo('mainmenu');
        this.hideSpinner();
    }

    public checkChangeLanguage(aNewLanguage: String, aCallback: Function): void {
        if (this.globalService.getSelectedLanguage() != aNewLanguage) {
            this.globalService.setLanguage(aNewLanguage);
            aCallback()
        }
    }

    public getSelectedLanguage(): String {
        return this.globalService.getSelectedLanguage();
    }

    public getSupportedLanguages(): Map<String, String> {
        return this.globalService.getSupportedLanguages();
    }

    public registerNewUser(): void {
        this.globalService.goTo('registration');
    }
}
