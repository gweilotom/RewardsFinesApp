import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../../base/components/base.component';
import { LoginService } from '../services/login.service';

@Component({
    selector: 'login-component',
    templateUrl: '../views/login.component.html'
})
export class LoginComponent extends BaseComponent {

    public languageLabel: String;
    public userEmailLabel: String;
    public passwordLabel: String;
    public loginLabel: String;
    public registerLabel: String;

    public supportedLanguages: Map<String, String>;

    public hide: boolean;

    public loginForm: FormGroup;
    public languageForm: FormGroup;

    public loadViewLabels(): void {
        this.languageLabel = this.loginService.getTranslation('LANGUAGE_FIELD');
        this.userEmailLabel = this.loginService.getTranslation('USEREMAIL_FIELD');
        this.passwordLabel = this.loginService.getTranslation('PASSWORD_FIELD');
        this.loginLabel = this.loginService.getTranslation('LOGIN_BUTTON');
        this.registerLabel = this.loginService.getTranslation('REGISTER_LINK');
    }

    public initViewElements(): void {
        this.loginService.setTitle('APP_TITLE');
        this.supportedLanguages = this.loginService.getSupportedLanguages();
        this.loginService.setLoggedIn(false);
        this.loginForm = this.formBuilder.group({
            useremail: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.languageForm = this.formBuilder.group({
            language: [this.loginService.getSelectedLanguage(), Validators.required]
        });

        this.languageForm.get('language').valueChanges.subscribe(val => {
            this.changeLanguage(val);
        })

        this.hide = false;
    }

    constructor(
        protected loginService: LoginService,
        private formBuilder: FormBuilder
    ) {
        super(loginService);
    }

    public executeLogin(): void {
        if (this.loginForm.get('useremail').valid && this.loginForm.get('password').valid) {
            this.loginService.login(this.loginForm);
        }
    }

    public enterPressed(keyCode: number): void {
        if (keyCode === 13) {
            this.executeLogin();
        }
    }

    public changeLanguage(aNewLanguage: String): void {
        this.loginService.checkChangeLanguage(aNewLanguage, () => this.loadViewLabels());
    }

    public registerNewUser(): void {
        this.loginService.registerNewUser();
    }
}
