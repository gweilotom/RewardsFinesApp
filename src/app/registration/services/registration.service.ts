import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { GlobalService } from "src/app/global/services/global.service";
import { BaseService } from "../../base/services/base.service";

@Injectable({
    providedIn: 'root'
})
export class RegistrationService extends BaseService {

    private salt = '1.ORSCLinz';

    constructor(
        protected globalService: GlobalService,
        private http: HttpClient
    ) {
        super(globalService);
    }

    public getSelectedLanguage(): String {
        return this.globalService.getSelectedLanguage();
    }

    public getSupportedLanguages(): Map<String, String> {
        return this.globalService.getSupportedLanguages();
    }

    public checkChangeLanguage(aNewLanguage: String, aCallback: Function): void {
        if (this.getSelectedLanguage() !== aNewLanguage) {
            this.globalService.setLanguage(aNewLanguage);
            aCallback();
        }
    }

    public checkCreateNewUser(aNewUserForm: FormGroup, aCallback: Function): void {
        const request = this.globalService.getWebServiceUrl() + "createUser";
        const payload: any = {
            username: aNewUserForm.get('username').value.toString(),
            name: aNewUserForm.get('firstname').value.toString(),
            surname: aNewUserForm.get('surname').value.toString(),
            password: btoa(aNewUserForm.get('password').value + ':' + this.salt).toString(),
            executive: false,
            email: aNewUserForm.get('email').value.toString(),
            language: aNewUserForm.get('preferredLanguage').value.toString()
        }
        this.http.post<JSON>(request, payload).subscribe(data => {
            console.log(data);
            aCallback(true);
        }, error => {
            console.log('Error occurres while registering new user: ', error);
            aCallback(false);
        });
    }

    public goToLogin(): void {
        this.globalService.goTo('login');
    }
}
