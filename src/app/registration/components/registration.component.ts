import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RegistrationService } from '../services/registration.service';
import { MustMatch } from '../../global/validators/custom.validators';

@Component({
    selector: "registration-component",
    templateUrl: "../views/registration.component.html"
})
export class RegistrationComponent {

    public firstNameField: String;
    public surnameField: String;
    public usernameField: String;
    public emailField: String;
    public passwordField: String;
    public confirmPasswordField: String;
    public preferredLanguageField: String;
    public registerButton: String;
    public backButton: String;
    public invalidEmailError: String;
    public passwordMatchError: String;
    public passwordLengthError: String;

    public newUserInputForm: FormGroup;

    public supportedLanguages: Map<String, String>;

    public hidePW: boolean;
    public hideConfirm: boolean;

    constructor(
        private registrationService: RegistrationService,
        private formBuilder: FormBuilder
    ) {
        this.reInitView();

        this.newUserInputForm = this.formBuilder.group({
            firstname: new FormControl('', [Validators.required]),
            surname: new FormControl('', [Validators.required]),
            username: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
            password: new FormControl('', [Validators.required, Validators.minLength(8)]),
            confirmPassword: new FormControl('', [Validators.required]),
            preferredLanguage: new FormControl(this.registrationService.getSelectedLanguage())
        }, {
            validators: MustMatch('password', 'confirmPassword')
        });

        this.newUserInputForm.get('firstname').valueChanges.subscribe(val => {
            if (val !== undefined || val !== null || val !== '') {
                this.buildUsername();
            }
        });
        this.newUserInputForm.get('surname').valueChanges.subscribe(val => {
            if (val !== undefined || val !== null || val !== '') {
                this.buildUsername();
            }
        });
        this.newUserInputForm.get('preferredLanguage').valueChanges.subscribe(val => {
            if (val !== this.registrationService.getSelectedLanguage()) {
                this.changeLanguage(val);
            }
        });

        this.supportedLanguages = this.registrationService.getSupportedLanguages();

        this.hidePW = false;
        this.hideConfirm = false;
    }

    public reInitView(): void {
        this.registrationService.setTitle('REGISTER_NEW_USER');

        this.firstNameField = this.registrationService.getTranslation('FIRSTNAME_FIELD');
        this.surnameField = this.registrationService.getTranslation('SURNAME_FIELD');
        this.usernameField = this.registrationService.getTranslation('USERNAME_FIELD');
        this.emailField = this.registrationService.getTranslation('EMAIL_FIELD');
        this.passwordField = this.registrationService.getTranslation('PASSWORD_FIELD');
        this.confirmPasswordField = this.registrationService.getTranslation('CONFIRM_PASSWORD_FIELD');
        this.preferredLanguageField = this.registrationService.getTranslation('PREFERRED_LANGUAGE_FIELD');
        this.registerButton = this.registrationService.getTranslation('REGISTER_BUTTON')
        this.backButton = this.registrationService.getTranslation('BACK_BUTTON');
        this.invalidEmailError = this.registrationService.getTranslation('INVALID_EMAIL_ERROR');
        this.passwordMatchError = this.registrationService.getTranslation('PASSWORD_MATCH_ERROR');
        this.passwordLengthError = this.registrationService.getTranslation('PASSWORD_LENGTH_ERROR');
    }

    public getSupportedLanguages(): Map<String, String> {
        return this.registrationService.getSupportedLanguages();
    }

    public changeLanguage(aNewLanguage: String): void {
        this.registrationService.checkChangeLanguage(aNewLanguage, () => this.reInitView());
    }

    public enterPressed(keyCode: number): void {
        if (keyCode === 13) {
            this.registerNewUser();
        }
    }

    public checkValidEmail(): boolean {
        if (this.newUserInputForm.get('email').errors !== null && !this.newUserInputForm.get('email').errors.required) {
            return false;
        }
        return true;
    }

    public checkPasswordLength(): boolean {
        if (this.newUserInputForm.get('password').errors !== null && !this.newUserInputForm.get('password').errors.required){
            return false;
        }
        return true;
    }

    public checkPasswordsMatch(): boolean {
        if (this.newUserInputForm.get('confirmPassword').errors !== null && this.newUserInputForm.get('confirmPassword').errors.mustMatch) {
            return false;
        }
        return true;
    }

    public registerNewUser(): void {
        if (this.newUserInputForm.valid) {
            this.registrationService.showSpinner();
            this.registrationService.checkCreateNewUser(this.newUserInputForm, (success) => this.newUserRegistered(success));
        }
    }

    public newUserRegistered(success: boolean): void {
        if (success) {
            this.registrationService.hideSpinner();
            this.goToLogin();
        }
    }

    public goToLogin(): void {
        this.registrationService.goToLogin();
    }

    public buildUsername(): void {
        if (this.newUserInputForm.get('username').pristine) {
            this.newUserInputForm.get('username').setValue(this.newUserInputForm.get('firstname').value.substring(0, 1) + this.newUserInputForm.get('surname').value);
        }
    }
}
