import { Component } from '@angular/core';
import { GlobalService } from './global/services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'RewardsFinesApp';

  public toolbarTitle: String;
  public logOffLabel: String;

  private initComplete: boolean;

  constructor(
    private globalService: GlobalService
  ) {
    this.initComplete = false;
    this.toolbarTitle = 'R.C. Linz'
    this.logOffLabel = 'Log off';
    this.globalService.initAppData((success) => this.initAppComplete(success));
  }

  public initAppComplete(aSuccess: boolean): void {
    this.initComplete = aSuccess;
    if (this.initComplete) {
      this.logOffLabel = this.globalService.getTranslation('LOGOFF_BUTTON');
      this.globalService.goTo('login');
    }
  }

  public isInitComplete(): boolean {
    return this.initComplete;
  }

  public titleAvailable(): boolean {
    if (this.globalService.getTitle() != undefined && this.globalService.getTitle() != null && this.globalService.getTitle() != '') {
      this.toolbarTitle = this.globalService.getTitle();
      return true;
    }
    this.toolbarTitle = 'R.C. Linz';
    return false;
  }

  public isLoggedIn(): boolean {
    return this.globalService.isLoggedIn();
  }

  public logOff(): void {
    this.globalService.setLoggedIn(false);
    this.globalService.goTo('login');
  }
}
