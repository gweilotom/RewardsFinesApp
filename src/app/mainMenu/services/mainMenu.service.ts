import { Injectable } from '@angular/core';
import { BaseService } from '../../base/services/base.service';
import { GlobalService } from '../../global/services/global.service';

@Injectable({
    providedIn: 'root'
})
export class MainMenuService extends BaseService {

    constructor (protected globalService: GlobalService) {
        super(globalService);
    }
}