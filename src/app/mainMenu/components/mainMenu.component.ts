import { Component } from '@angular/core';
import { BaseComponent } from '../../base/components/base.component';
import { MainMenuService } from '../services/mainMenu.service';

@Component({
    selector: 'mainmenu-component',
    templateUrl: '../views/mainMenu.component.html'
})
export class MainMenuComponent extends BaseComponent {

    public leaderboardLabel: String;
    public finesOverviewLabel: String;
    public rewardsOverviewLabel: String;
    public selfFineLabel: String;
    public finePlayerLabel: String;
    public rewardPlayerLabel: String;
    public finesLabel: String;
    public rewardsLabel: String;
    public addPlayerLabel: String;
    public createFineLabel: String;
    public createRewardLabel: String;

    public loadViewLabels(): void {
        this.leaderboardLabel = this.mainMenuService.getTranslation('LEADERBOARD');
        this.finesOverviewLabel = this.mainMenuService.getTranslation('FINES_OVERVIEW');
        this.rewardsOverviewLabel = this.mainMenuService.getTranslation('REWARDS_OVERVIEW');
        this.selfFineLabel = this.mainMenuService.getTranslation('SELF_FINE');
        this.finePlayerLabel = this.mainMenuService.getTranslation('FINE_PLAYER');
        this.rewardPlayerLabel = this.mainMenuService.getTranslation('REWARD_PLAYER');
        this.finesLabel = this.mainMenuService.getTranslation('FINES');
        this.rewardsLabel = this.mainMenuService.getTranslation('REWARDS');
        this.addPlayerLabel = this.mainMenuService.getTranslation('ADD_PLAYER');
        this.createFineLabel = this.mainMenuService.getTranslation('CREATE_FINE');
        this.createRewardLabel = this.mainMenuService.getTranslation('CREATE_REWARD');
    }

    public initViewElements(): void {
        this.mainMenuService.setTitle('MAIN_MENU_TITLE');
    }

    constructor (
        protected mainMenuService: MainMenuService
    ) {
        super(mainMenuService);
    }

    public isExecutive(): boolean {
        return this.mainMenuService.isExecutive();
    }

    public openLeaderboard(): void {}
    
    public openFinesOverview(): void {}

    public openRewardsOverview(): void {}

    public finePlayer(aSelf: boolean): void {}

    public rewardPlayer(): void {}

    public showFines(): void {}

    public showRewards(): void {}

    public addPlayer(): void {}

    public createNewFine(): void {}

    public createNewReward(): void {}
}