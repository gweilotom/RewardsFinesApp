import { GlobalService } from 'src/app/global/services/global.service';

export abstract class BaseService {

    constructor (protected globalService: GlobalService) {}

    public setTitle(aTitleKey: String): void {
        this.globalService.setTitle(this.getTranslation(aTitleKey));
    }

    public isLoggedIn(): void {
        if (!this.globalService.isLoggedIn()) {
            this.globalService.goTo('login');
        }
    }

    public isExecutive(): boolean {
        return this.globalService.isExecutive();
    }

    public getTranslation(aKey: String, aLanguage = this.globalService.getSelectedLanguage()): String {
        return this.globalService.getTranslation(aKey, aLanguage);
    }

    public showSpinner(): void {
        this.globalService.showSpinner();
    }

    public hideSpinner(): void {
        this.globalService.hideSpinner();
    }
}
