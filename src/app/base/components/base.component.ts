import { Injectable, OnDestroy, OnInit } from '@angular/core';

@Injectable()
export abstract class BaseComponent implements OnInit, OnDestroy {

    ngOnInit(): void {
        this.baseService.isLoggedIn();
        this.loadViewLabels();
        this.initViewElements();
    }

    ngOnDestroy(): void {}

    public abstract loadViewLabels(): void;

    public abstract initViewElements(): void;

    constructor(protected baseService) {}
}
