import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';

import { GlobalService } from './global/services/global.service';
import { LanguageService } from './global/services/language.service';
import { ConfigService } from './global/services/config.service';

import { SpinnerOverlayComponent } from './global/overlay/components/spinner.overlay.component';
import { LoginComponent } from './login/components/login.component';
import { MainMenuComponent } from './mainMenu/components/mainMenu.component';
import { RegistrationComponent } from './registration/components/registration.component';

import { OverlayService } from './global/overlay/services/overlay.service';
import { LoginService } from './login/services/login.service';
import { MainMenuService } from './mainMenu/services/mainMenu.service';
import { RegistrationService } from './registration/services/registration.service';

@NgModule({
  declarations: [
    AppComponent,
    SpinnerOverlayComponent,
    LoginComponent,
    MainMenuComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,

    MatToolbarModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule,

    OverlayModule
  ],
  providers: [
    GlobalService,
    ConfigService,
    LanguageService,
    OverlayService,
    LoginService,
    MainMenuService,
    RegistrationService
  ],
  bootstrap: [
    AppComponent,
    SpinnerOverlayComponent,
    LoginComponent,
    MainMenuComponent,
    RegistrationComponent
  ]
})
export class AppModule { }
